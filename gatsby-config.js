/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  siteMetadata: {
    title: `portfolio by gatsby`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [],
}
